import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import PageNotFoundComponent from './page.not.pound';
import ListComponent from './list';
import DetailsComponent from './details';

const appRoutes: Routes = [
  { path: '',   redirectTo: '/weathers', pathMatch: 'full' },
  { path: 'weathers',  component: ListComponent },
  { path: 'weather/:id', component: DetailsComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export default class RoutingModule { }
