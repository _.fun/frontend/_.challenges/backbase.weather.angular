import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';

import AppComponent from './index';
import PageNotFoundComponent from './page.not.pound';
import ListComponent from './list';
import DetailsComponent from './details';

import RoutingModule from './routing.module';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ListComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    RoutingModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
