import { Component } from '@angular/core';

@Component({
  selector: 'app-city',
  template: `<div>Not found</div>`
})
export default class PageNotFoundComponent {
  title = 'city';
}
