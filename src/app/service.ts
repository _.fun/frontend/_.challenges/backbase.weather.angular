import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

export class City {
  constructor(public id: number, public name: string) { }
}

const Cities = [
  new City(11, 'London'),
  new City(12, 'Talin'),
  new City(13, 'Vienna'),
  new City(14, 'Rome'),
  new City(15, 'Moskow')
];

@Injectable()
export default class WeatherService {
  get() { return of(Cities); }

  getById(id: number | string) {
    return this.get().pipe(
      // (+) before `id` turns the string into a number
      map(cities => cities.find(({ id: cityId }) => cityId === +id))
    );
  }
}
