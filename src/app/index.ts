import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './template.html'
})
export default class AppComponent {
  title = 'app';
}
